package com.island.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.island.entity.Role;

public class RoleDaoImpl implements RoleDao{
    
	private static final String SQL_QUERY_ROLE = "SELECT * FROM ROLE ORDER BY ROLE_ID";
	private static final String SQL_UPDATE_ROLE = "UPDATE ROLE r SET r.ROLE_NAME = ? WHERE r.ROLE_ID = ? ";
	private static final String SQL_INSERT_ROLE = "INSERT INTO ROLE(ROLE_ID, ROLE_NAME) VALUES(?, ?)";
	private static final String SQL_QUERY_PERSONOFROLE = "SELECT * FROM PERSON_ROLE_RELATION WHERE ROLE_ID = ?";
	private static final String SQL_DELETE_ROLE = "DELETE FROM ROLE WHERE ROLE_ID = ? ";
	
	/**
	 * 取出 DB 中所有 roles 
	 */
	@Override
	public ArrayList<Role> getAllRoles(Connection connection) throws SQLException{
		ArrayList<Role> roleList = new ArrayList<Role>();
		Role role = null;
		PreparedStatement st = null;
    	ResultSet roleResults = null;
    	
    	st = connection.prepareStatement(SQL_QUERY_ROLE);
    	roleResults = st.executeQuery();	
    	while(roleResults.next()){
    		role = new Role();
    		role.setRoleId(roleResults.getString("ROLE_ID"));
    		role.setRoleName(roleResults.getString("ROLE_NAME"));

    		roleList.add(role);
    	}
    	closeStatement(st);
		return roleList;
	}
	/**
     * 更新 role 
     */
	@Override
	public void updateRole(Connection connection,String roleId,String roleName) throws SQLException{

		PreparedStatement st = null;
    	st = connection.prepareStatement(SQL_UPDATE_ROLE);
    	st.setString(1, roleName);
    	st.setString(2, roleId);
    	st.execute();
    	
    	closeStatement(st);
    
	}
	/**
     * 新增 role 
     */
	@Override
	public void insertRole(Connection connection,String roleId,String roleName) throws SQLException{

		PreparedStatement st = null;
    	st = connection.prepareStatement(SQL_INSERT_ROLE);
    	st.setString(1, roleId);
    	st.setString(2, roleName);
    	st.execute();
    	
    	closeStatement(st);
    
	}
	/**
     * 檢查是否有  person 屬於此 role ,true = this role has person
     */
	@Override
	public boolean checkDelete(Connection connection,String roleId) throws SQLException{
		boolean result =false; 
		PreparedStatement statement = null;
    	ResultSet resultSet = null;
    	
    	statement = connection.prepareStatement(SQL_QUERY_PERSONOFROLE);
    	statement.setString(1, roleId);
    	
    	resultSet = statement.executeQuery();
    	
    	if (resultSet.next()){
    		result = true;
    	}
    	
    	closeStatement(statement);
		return result;
		
	}
	/**
     * 刪除 role 
     */
	@Override
	public void deleteRole(Connection connection,String roleId) throws SQLException{
		
		PreparedStatement statement = null;
		statement = connection.prepareStatement(SQL_DELETE_ROLE);
    	statement.setString(1, roleId);
    	statement.execute();
    	
    	closeStatement(statement);
	}
	
	protected final static void closeStatement(PreparedStatement statement) {
		if (null != statement)
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException("close statement failed", e);
			}
	}
}

package com.island.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import javax.print.DocFlavor.URL;

import com.island.entity.DBInfo;

public class ConnectionManager {
    /**取得 resources 內 .properties file 的 db 連線資訊*/
    private DBInfo getDBInfo() {
       DBInfo dbInfo = new DBInfo();
        
        Properties properties = new Properties();
        try {
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        dbInfo.setDriver(properties.getProperty("jdbc.driver"));
        dbInfo.setDbUrl(properties.getProperty("jdbc.url"));
        dbInfo.setUserName(properties.getProperty("jdbc.username"));
        dbInfo.setPassword(properties.getProperty("jdbc.password"));
        return dbInfo;
    }
    /**取得 connection*/
    public Connection getConnection() throws SQLException {
        DBInfo dbInfo = new DBInfo();
        try {
            dbInfo  = getDBInfo();
            Class.forName(dbInfo.getDriver());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url = dbInfo.getDbUrl();
        String user = dbInfo.getUserName();
        String pass = dbInfo.getPassword();

        return DriverManager.getConnection(url, user, pass);
    }

    public static void closeConnection(Connection connection) {
        closeConnection(connection, true);
    }

    protected static void closeConnection(Connection connection, boolean setAutoCommit) {
        if (null != connection)
            try {
                connection.setAutoCommit(setAutoCommit);
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException("close connection failed", e);
            }
    }

    protected static void closeStatement(PreparedStatement statement) {
        if (null != statement)
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException("close statement failed", e);
            }
    }

}
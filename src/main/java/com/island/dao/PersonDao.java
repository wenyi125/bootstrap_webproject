package com.island.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import com.island.entity.Person;
import com.island.entity.Role;

public interface PersonDao {

	public void insertPerson(String personId, String firstName, String lastName, Connection connection) throws SQLException;
	public void updatePerson(String personId, String firstName, String lastName, Connection connection) throws SQLException;
	public void insertPersonRolesRelationship(String personId, List<String> roleIds, Connection connection) throws SQLException;
	public List<Person> getAllPersons(Connection connection) throws SQLException;
	public void deletePerson(Connection connection,String personId) throws SQLException;
	public List<Role> getPersonRoles(Connection connection,String personId)throws SQLException;
}

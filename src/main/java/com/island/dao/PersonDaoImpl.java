package com.island.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.island.entity.Person;
import com.island.entity.PersonRoleRelation;
import com.island.entity.Role;

public class PersonDaoImpl implements PersonDao {
    /** 關於 insert person 的 SQL */
    private static final String SQL_INSERT_PERSON = "INSERT INTO PERSON(PERSON_ID, LAST_NAME, FIRST_NAME) VALUES(?, ?, ?)";
    private static final String SQL_INSERT_PERSON_ROLE_RELATION = "INSERT INTO PERSON_ROLE_RELATION(PERSON_ID, ROLE_ID) VALUES(?, ?); ";
    private static final String SQL_QUERY_PERSON_LIST = "SELECT * FROM PERSON ORDER BY PERSON_ID";
    private static final String SQL_QUERY_PERSON_ROLE_RELATION = "SELECT t1.*, t2.ROLE_NAME FROM PERSON_ROLE_RELATION t1, ROLE t2 WHERE t1.ROLE_ID = t2.ROLE_ID AND t1.PERSON_ID = ? ";
    /** 關於 delete person 的 SQL */
    private static final String SQL_DELETE_PERSON = "DELETE FROM PERSON WHERE PERSON_ID= ?";
    private static final String SQL_DELETE_PERSON_ROLE = "DELETE FROM PERSON_ROLE_RELATION WHERE PERSON_ID= ?";
    /** 關於 maintain person 的 SQL */
    private static final String SQL_UPDATE_PERSON = "UPDATE PERSON p SET p.LAST_NAME = ?, p.FIRST_NAME = ? WHERE p.PERSON_ID = ? ";

    /**
     * 新增 person
     */
    @Override
    public void insertPerson(String personId, String firstName, String lastName, Connection connection)
            throws SQLException {
        PreparedStatement statement = null;

        statement = connection.prepareStatement(SQL_INSERT_PERSON);
        statement.setString(1, personId);
        statement.setString(2, lastName);
        statement.setString(3, firstName);
        statement.execute();
        closeStatement(statement);
    }

    /**
     * 更新 person
     */
    @Override
    public void updatePerson(String personId, String firstName, String lastName, Connection connection)
            throws SQLException {
        PreparedStatement statement = null;

        statement = connection.prepareStatement(SQL_UPDATE_PERSON);
        statement.setString(1, lastName);
        statement.setString(2, firstName);
        statement.setString(3, personId);
        statement.execute();

        statement = connection.prepareStatement(SQL_DELETE_PERSON_ROLE);
        statement.setString(1, personId);
        statement.execute();
        
        closeStatement(statement);
    }

    /**
     * 新增 personId 和相對應的 roles
     */
    @Override
    public void insertPersonRolesRelationship(String personId, List<String> roleIds, Connection connection)
            throws SQLException {
        String roleId = null;
        PreparedStatement statement = null;

        statement = connection.prepareStatement(SQL_INSERT_PERSON_ROLE_RELATION);

        for (int a = 0; a < roleIds.size(); a++) {
            roleId = roleIds.get(a);
            statement.setString(1, personId);
            statement.setString(2, roleId);
            statement.execute();
        }
        closeStatement(statement);

    }

    /**
     * 取出該 person 的 roles
     */
    @Override
    public List<Role> getPersonRoles(Connection connection, String personId) throws SQLException {
        List<Role> roles = new ArrayList<Role>();
        Role role = new Role();
        PreparedStatement st = null;
        ResultSet roleResults = null;

        st = connection.prepareStatement(SQL_QUERY_PERSON_ROLE_RELATION);
        st.setString(1, personId);
        roleResults = st.executeQuery();

        while (roleResults.next()) {
            role = new Role();
            role.setRoleId(roleResults.getString("ROLE_ID"));
            role.setRoleName(roleResults.getString("ROLE_NAME"));

            roles.add(role);
        }

        return roles;
    }

    /**
     * 取出 DB 中所有 persons
     */
    @Override
    public List<Person> getAllPersons(Connection connection) throws SQLException {
        List<Person> personList = new ArrayList<Person>();
        Person person = null;
        PreparedStatement st = null;
        ResultSet personResults = null;

        st = connection.prepareStatement(SQL_QUERY_PERSON_LIST);
        personResults = st.executeQuery();
        while (personResults.next()) {
            person = new Person();
            person.setPersonId(personResults.getString("PERSON_ID"));
            person.setLastName(personResults.getString("LAST_NAME"));
            person.setFirstName(personResults.getString("FIRST_NAME"));

            generateCorrespondingRoles(connection, person);
            personList.add(person);
        }
        return personList;

    }

    /**
     * 刪除 person
     */
    @Override
    public void deletePerson(Connection connection, String personId) throws SQLException {
        PreparedStatement statement = null;

        connection.setAutoCommit(false);
        statement = connection.prepareStatement(SQL_DELETE_PERSON);
        statement.setString(1, personId);
        statement.execute();

        statement = connection.prepareStatement(SQL_DELETE_PERSON_ROLE);
        statement.setString(1, personId);
        statement.execute();

        closeStatement(statement);
    }

    /**
     * 生成該 person 所對應到的 roles，用在 getAllPersons
     */
    private void generateCorrespondingRoles(Connection connection, Person person) throws SQLException {
        Role role = null;
        PersonRoleRelation roleRelationship = null;
        List<PersonRoleRelation> roleRelationships = null;
        PreparedStatement statement4Role = null;
        ResultSet roleResults = null;

        statement4Role = connection.prepareStatement(SQL_QUERY_PERSON_ROLE_RELATION);
        statement4Role.setString(1, person.getPersonId());
        roleResults = statement4Role.executeQuery();

        roleRelationships = new ArrayList<PersonRoleRelation>();
        while (roleResults.next()) {
            role = new Role();
            role.setRoleId(roleResults.getString("ROLE_ID"));
            role.setRoleName(roleResults.getString("ROLE_NAME"));

            roleRelationship = new PersonRoleRelation();
            roleRelationship.setRole(role);
            roleRelationships.add(roleRelationship);
        }
        person.setPersonRoleRelation(roleRelationships);
    }

    /**
     * closeStatement
     */
    protected final static void closeStatement(PreparedStatement statement) {
        if (null != statement)
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException("close statement failed", e);
            }
    }

}

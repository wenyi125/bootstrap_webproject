package com.island.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.island.entity.Role;

public interface RoleDao {
	public ArrayList<Role> getAllRoles(Connection connection) throws SQLException;
	public void updateRole(Connection connection,String roleId,String roleName) throws SQLException;
	public void insertRole(Connection connection,String roleId,String roleName) throws SQLException;
	public boolean checkDelete(Connection connection,String roleId) throws SQLException;
	public void deleteRole(Connection connection,String roleId) throws SQLException;
	

}

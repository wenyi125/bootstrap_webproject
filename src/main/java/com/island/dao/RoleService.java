package com.island.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.island.entity.ResponseBean;
import com.island.entity.Role;

public class RoleService {

    private RoleDaoImpl roledao = new RoleDaoImpl();
    private ConnectionManager CM = new ConnectionManager();
    private Gson gson = new Gson();

    /**
     * 新增 role
     * @param roleId
     *            欲新增 role 的 Id
     * @param roleName
     *            欲新增 role 的 Name
     * @return ResponseBean 內含新增狀態成功或失敗，若失敗附上錯誤訊息
     */
    public ResponseBean insertRole(String roleId, String roleName) {
        Connection connection = null;
        ResponseBean responseBean = new ResponseBean();
        try {
            connection = CM.getConnection();
            connection.setAutoCommit(false);
            roledao.insertRole(connection, roleId, roleName);
            responseBean.setStatus(responseBean.statusSuccess);

        } catch (SQLException e) {
            String eroMesg = "insert role failed";
            responseBean.setStatus(responseBean.statusFail);
            responseBean.setExceptionMessage(eroMesg + ": " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(eroMesg, e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        return responseBean;

    }

    /**
     * 刪除指定 role
     * 
     * @param roleId
     *            欲刪除 role 之 Id
     * @return ResponseBean 內含刪除狀態成功或失敗，若失敗附上錯誤訊息
     */
    public ResponseBean deleteRole(String roleId) {
        Connection connection = null;
        ResponseBean responseBean = new ResponseBean();
        try {
            connection = CM.getConnection();
            connection.setAutoCommit(false);
            roledao.deleteRole(connection, roleId);
            responseBean.setStatus(responseBean.statusSuccess);

        } catch (SQLException e) {
            String eroMesg = "delete role failed";
            responseBean.setStatus(responseBean.statusFail);
            responseBean.setExceptionMessage(eroMesg + ": " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(eroMesg, e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        return responseBean;
    }

    /**
     * 取出 DB 中所有 persons
     * 
     * @return ResponseBean 內含所有 roles 內容、取出狀態成功或失敗，若失敗附上訊息
     */
    public ResponseBean getAllRoles() {
        Connection connection = null;
        ResponseBean responseBean = new ResponseBean();
        ArrayList<Role> roles = new ArrayList<Role>();
        try {
            connection = CM.getConnection();
            roles = roledao.getAllRoles(connection);
            String rolesToJson = gson.toJson(roles);
            responseBean.setStatus(responseBean.statusSuccess);
            responseBean.setData(rolesToJson);
        } catch (SQLException e) {
            String eroMesg = "get list of roles failed";
            responseBean.setStatus(responseBean.statusFail);
            responseBean.setExceptionMessage(eroMesg + ": " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(eroMesg, e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        return responseBean;
    }

    /**
     * 更新 role
     * 
     * @param roleId
     *            欲更新的 role 的 Id
     * @param roleName
     *            欲更新的 roleName
     * @return ResponseBean 內含更新狀態成功或失敗，若失敗附上訊息
     */
    public ResponseBean updateRole(String roleId, String roleName) {
        Connection connection = null;
        ResponseBean responseBean = new ResponseBean();
        try {
            connection = CM.getConnection();
            connection.setAutoCommit(false);
            roledao.updateRole(connection, roleId, roleName);

            responseBean.setStatus(responseBean.statusSuccess);

        } catch (SQLException e) {
            String eroMesg = "update role failed";
            responseBean.setStatus(responseBean.statusFail);
            responseBean.setExceptionMessage(eroMesg + ": " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(eroMesg, e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        return responseBean;
    }

    /**
     * 檢查有無 persons 屬於該 role
     * 
     * @param roleId
     *            欲查詢 role 的 Id
     * @return true or false; true: This role contains person
     */
    public boolean checkDeleteRole(String roleId) {
        Connection connection = null;
        try {
            boolean result = false;
            connection = CM.getConnection();
            result = roledao.checkDelete(connection, roleId);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("check delete role failed", e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }

    }

}

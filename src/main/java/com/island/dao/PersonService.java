package com.island.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.island.dao.PersonDaoImpl;
import com.island.entity.Person;
import com.island.entity.ResponseBean;
import com.island.entity.Role;

public class PersonService {

    private PersonDao persondao = new PersonDaoImpl();
    private ConnectionManager CM = new ConnectionManager();
    private Gson gson = new Gson();

    /**
     * 新增 person
     * 
     * @param person
     *            欲新增 person 的資訊
     * @param roleIds
     *            該 person 的 roles
     * @return ResponseBean 內含新增狀態成功或失敗，若失敗附上錯誤訊息
     */
    public ResponseBean insertPerson(Person person, List<String> roleIds) {
        ResponseBean responseBean = new ResponseBean();
        Connection connection = null;
        try {
            connection = CM.getConnection();
            connection.setAutoCommit(false);
            persondao.insertPerson(person.getPersonId(), person.getFirstName(), person.getLastName(), connection);
            persondao.insertPersonRolesRelationship(person.getPersonId(), roleIds, connection);
            responseBean.setStatus(responseBean.statusSuccess);

        } catch (SQLException e) {
            String errMesg = "insert person failed";
            responseBean.setStatus(responseBean.statusFail);
            responseBean.setExceptionMessage(errMesg + ": " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(errMesg, e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        return responseBean;
    }

    /**
     * 刪除指定 person
     * 
     * @param personId
     *            欲刪除 person 之 Id
     * @return ResponseBean 內含刪除狀態成功或失敗，若失敗附上錯誤訊息
     */
    public ResponseBean deletePerson(String personId) {
        ResponseBean responseBean = new ResponseBean();
        Connection connection = null;
        try {
            connection = CM.getConnection();
            connection.setAutoCommit(false);
            persondao.deletePerson(connection, personId);
            responseBean.setStatus(responseBean.statusSuccess);
        } catch (SQLException e) {
            String errMesg = "delete person failed";
            responseBean.setStatus(responseBean.statusFail);
            responseBean.setExceptionMessage("delete person failed: " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(errMesg, e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        return responseBean;
    }

    /**
     * 從 DB 取出所有 persons
     * 
     * @return ResponseBean 內含所有 persons 內容、取出狀態成功或失敗，若失敗附上訊息
     */
    public ResponseBean getAllPersons() {
        ResponseBean responseBean = new ResponseBean();

        Connection connection = null;
        List<Person> persons = new ArrayList<Person>();
        try {
            connection = CM.getConnection();
            persons = persondao.getAllPersons(connection);
            String personsToGson = gson.toJson(persons);
            responseBean.setStatus(responseBean.statusSuccess);
            responseBean.setData(personsToGson);

        } catch (SQLException e) {
            responseBean.setStatus(responseBean.statusFail);
            responseBean.setExceptionMessage("get list of person failed: " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException("get list of person failed", e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        return responseBean;
    }

    /**
     * 更新 person
     * 
     * @param person
     *            欲更新的 person 內容
     * @param roleIds
     *            該 person 的 roles
     * @return ResponseBean 內含新增狀態成功或失敗，若失敗附上錯誤訊息
     */
    public ResponseBean updatePerson(Person person, List<String> roleIds) {
        ResponseBean responseBean = new ResponseBean();
        Connection connection = null;
        try {
            connection = CM.getConnection();
            connection.setAutoCommit(false);
            persondao.updatePerson(person.getPersonId(), person.getFirstName(), person.getLastName(), connection);
            persondao.insertPersonRolesRelationship(person.getPersonId(), roleIds, connection);

            responseBean.setStatus(responseBean.statusSuccess);

        } catch (SQLException e) {
            String errMesg = "maintain person failed";
            responseBean.setStatus(responseBean.statusFail);
            responseBean.setExceptionMessage(errMesg + ": " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(errMesg, e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        return responseBean;
    }

    /**
     * 取出該 person 對應到的 roles
     * 
     * @param personId
     *            指定的 personId
     * @return ResponseBean 內含 roles 內容、取出狀態成功或失敗，若失敗印出失敗訊息
     */
    public ResponseBean getPersonRoles(String personId) {
        ResponseBean responseBean = new ResponseBean();
        Connection connection = null;
        List<Role> roles = new ArrayList<Role>();
        try {
            connection = CM.getConnection();
            roles = persondao.getPersonRoles(connection, personId);
            String personRolesToGson = gson.toJson(roles);
            responseBean.setStatus(responseBean.statusSuccess);
            responseBean.setData(personRolesToGson);
        } catch (SQLException e) {
            String errMesg = "get person's roles failed";
            responseBean.setStatus(responseBean.statusFail);
            responseBean.setExceptionMessage(errMesg + ": " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(errMesg, e);
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        return responseBean;
    }

}

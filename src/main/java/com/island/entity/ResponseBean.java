package com.island.entity;

public class ResponseBean {
    public String statusSuccess = "Success";
    public String statusFail = "Fail";
    
    private String status;
    private String data;
    private String exceptionMessage;
    
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
    public String getExceptionMessage() {
        return exceptionMessage;
    }
    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

}

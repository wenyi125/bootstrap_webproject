
package com.island.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * A Role model.
 * @author Kyle
 *
 */
@XmlRootElement   
public class Role {
	
	private String roleId;
	private String roleName;
	
	/**
	 * @return the id
	 */
	public String getRoleId() {
		return roleId;
	}
	/**
	 * @param id the id to set
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	/**
	 * @return the name
	 */
	public String getRoleName() {
		return roleName;
	}
	/**
	 * @param name the name to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}

package com.island.resource;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.island.dao.PersonService;
import com.island.dao.RoleService;
import com.island.entity.Person;
import com.island.entity.ResponseBean;
import javax.ws.rs.GET;
import javax.ws.rs.POST;

@Path("/people")
@Consumes({ MediaType.APPLICATION_XML })
public class PeopleRestful {

    private PersonService personService = new PersonService();
    private Gson gson = new Gson();

    /**
     * 新增 person
     */
    @POST
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    public String createPerson(@FormParam("personId") String personId, @FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName, @FormParam("roleName") List<String> rolename) {
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setPersonId(personId);

        ResponseBean responseBean = personService.insertPerson(person, rolename);
        String insertPersonToJson = gson.toJson(responseBean);
        return insertPersonToJson;
    }

    /**
     * 删除 person
     */

    @Path("/delete/{personId}")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public String deletePersonTest(@PathParam("personId") String personId) {
        ResponseBean responseBean = personService.deletePerson(personId);
        String deleteToJson = gson.toJson(responseBean);

        return deleteToJson;
    }

    /**
     * 修改 person
     */

    @POST
    @Path("/maintain")
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    public String updatePerson(@FormParam("firstName") String firstName, @FormParam("lastName") String lastName,
            @FormParam("personId") String personId, @FormParam("roleName") List<String> rolename) {
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setPersonId(personId);
        ResponseBean responseBean = personService.updatePerson(person, rolename);
        String updateToJson = gson.toJson(responseBean);

        return updateToJson;
    }

    /**
     * 取得該 person 有哪些 roles
     */

    @GET
    @Path("/maintain/getPersonRoles/{personId}")
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    public String getPersonRoles(@PathParam("personId") String personId) {
        ResponseBean responseBean = personService.getPersonRoles(personId);
        String rolesToJson = gson.toJson(responseBean);
        return rolesToJson;
    }

    /**
     * 取得所有 persons
     */

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPersons() {
        ResponseBean responseBean = personService.getAllPersons();
        String personsToJson = gson.toJson(responseBean);

        return personsToJson;
    }

    /**
     * 取得現在所有的 roles
     */

    @GET
    @Path("/getRoles")
    @Produces(MediaType.APPLICATION_JSON)

    public String getAllRoles() {
        RoleService roleService = new RoleService();
        ResponseBean responseBean = roleService.getAllRoles();
        String roles = gson.toJson(responseBean);

        return roles;
    }
}

// @Produces 定義回傳 client 端的類型
// @Consumes 指定特定消耗某種資源的類型 一般用于post和put接收客户端参数
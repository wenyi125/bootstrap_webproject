package com.island.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.island.dao.RoleService;
import com.island.entity.ResponseBean;
import com.island.entity.Role;

@Path("/role")
@Consumes({ MediaType.APPLICATION_XML })
public class RoleRestful {

    private RoleService roleService = new RoleService();
    private Gson gson = new Gson();
    
    /**
     * 新增 person
     */
    @POST
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    public String createRole(@FormParam("roleId") String roleId, @FormParam("roleName") String roleName) {
        ResponseBean responseBean = new ResponseBean();
        responseBean = roleService.insertRole(roleId, roleName);
        String createRoleToJson = gson.toJson(responseBean);

        return createRoleToJson;
    }
    /**
     * 取得所有 roles
     */

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPersons() {
        ResponseBean responseBean = new ResponseBean();
        responseBean = roleService.getAllRoles();
        String rolesToJson = gson.toJson(responseBean);
        return rolesToJson;
    }
    /**
     * 修改 person
     */
    @POST
    @Path("/maintain")
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    @Produces(MediaType.TEXT_PLAIN)
    public String updateRole(@FormParam("roleId") String roleId, @FormParam("roleName") String roleName) {
        ResponseBean responseBean = new ResponseBean();
        Role role = new Role();
        role.setRoleName(roleName);
        role.setRoleId(roleId);
        responseBean = roleService.updateRole(roleId, roleName);
        String maintainRoleToJson = gson.toJson(responseBean);

        return maintainRoleToJson;
    }
    /**
     * 删除 person
     */
    @POST
    @Path("/delete/{roleId}")
    @Produces("application/json")
    public String deleteRoleTest(@PathParam("roleId") String roleId) {
        String resultMesg = "";
        ResponseBean responseBean = new ResponseBean();
        if (roleService.checkDeleteRole(roleId)) {
            resultMesg = "This role contains person, cann't delete";
            responseBean.setData(resultMesg);
            responseBean.setStatus(responseBean.statusFail);
        } else {
            resultMesg = "Deleted successfully!";
            responseBean = roleService.deleteRole(roleId);
            responseBean.setData(resultMesg);
        }
        String deletePersonToJson = gson.toJson(responseBean);
        return deletePersonToJson;
    }

}

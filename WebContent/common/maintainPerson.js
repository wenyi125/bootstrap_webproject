document
		.write("<script type='text/javascript' src='../common/feature.js'></script>");
document
		.write("<script type='text/javascript' src='../parameter.js'></script>");
/* 從 DB 取出現有的 roles 做成 checkbox */
(function($) {
	$(function() {
		var para = getPara();
		$("#personId").val(para[0][1]);
		$("#firstName").val(para[1][1]);
		$("#lastName").val(para[2][1]);
		$('#maintainPersonForm').attr('action', restful.peopleUpdate);

		$.ajax({
			type : "GET",
			async : false,
			url : restful.peopleGetRoles,
			dataType : "json",
			success : function(data, response, xhr) {
				if (xhr.status == "200" && data.status == "Success") {
					showRoles(data);
				} else {
					alert("status: " + xhr.status + ", " + xhr.statusText
							+ " ,exception message: " + data.exceptionMessage);
				}
			},
			error : function(xhr) {
				alert("err: " + xhr.status + ' ' + xhr.statusText);
			}
		});

		/* 選取該 person 的 roles 並在 checkbox 打勾 */
		$.ajax({
			type : "GET",
			async : false,
			dataType : "json",
			url : restful.peopleGetPersonRoles + para[0][1],
			success : function(data, response, xhr) {
				if (xhr.status == "200" && data.status == "Success") {
					var personRoles = JSON.parse(data.data);
					$.each(personRoles, function(key, value) {
						document.getElementById(value.roleId).checked = true
					});
				} else {
					alert("status: " + xhr.status + ", " + xhr.statusText
							+ " ,exception message: " + data.exceptionMessage);
				}
			},
			error : function(xhr) {
				alert("err: " + xhr.status + ' ' + xhr.statusText);
			}
		});
		/* 提交修改 person 的資料 */
		$("#maitainPersonSubmit").click(
				function() {
					if (personValidateForm()) {
						$.ajax({
							type : "POST",
							contentType : 'application/json',
							url : restful.peopleUpdate,
							data : $("#maintainPersonForm").serialize(),
							success : function(data, response, xhr) {
								if (xhr.status == "200"
										&& data.status == "Success") {
									location.href = 'personList.html';
								} else {
									alert("status: " + xhr.status + ", "
											+ xhr.statusText
											+ " ,exception message: "
											+ data.exceptionMessage);
								}
							},
							error : function(xhr) {
								alert("err: " + xhr.status + ' '
										+ xhr.statusText);
							}
						});
					}
				});

		$("#restButton").click(function() {
			formReset();
		});
	});

}(jQuery));

function formReset() {
	var para = getPara();
	$("#personId").val(para[0][1]);
	$("#firstName").val("");
	$("#lastName").val("");
	var checkboxes = $('[name="roleName"]');
	for (i = 0; i < checkboxes.length; i++) {
		checkboxes[i].checked = false;
	}
}

/*取出 URL 傳來的參數*/
function getPara() {
	var urlinfo = location.href;
	var offset = urlinfo.indexOf("?");
	var paraList = urlinfo.substr(offset + 1, urlinfo.length)
	var paraArray = paraList.split("&");
	var para = new Array();
	for (i = 0; i < paraArray.length; i++) {
		para[i] = paraArray[i].split("=");
	}
	return para;
}
/*列出 DB 中的 roles*/
function showRoles(data){
	var container = $('#rolesGroup');
	var roles = JSON.parse(data.data);
	$.each(roles, function(key, value) {
		$('<input />', {
			type : 'checkbox',
			id : value.roleId,
			value : value.roleId,
			name : 'roleName'
		}).appendTo(container);
		$('<label />', {
			'for' : value.roleId,
			text : value.roleName
		}).appendTo(container);
	});
}
/*檢查每個欄位是否有填寫, person*/
function personValidateForm() {
	var pv = $("#personId").val();
	var fv = $("#firstName").val();
	var lv = $("#lastName").val();
	var $checkboxes = $('[name="roleName"]');
	var check = true;
	for (i = 0; i < $checkboxes.length; i++) {
		if ($checkboxes[i].checked == true) {
			check = false;
		}
	}
	if (pv == null || pv == "", fv == null || fv == "", lv == null || lv == "", check) {
		alert("Please Fill All Required Field");
		return false;
	} else {
		return true;
	}
}

/*檢查每個欄位是否有填寫, role*/
function roleValidateForm() {
	var riv = $("#roleId").val();
	var rnv = $("#roleName").val()

	if (riv == null || riv == "" || rnv == null || rnv == "") {
		alert("Please Fill All Required Field");
		return false;
	}else {
		return true;
	}
}

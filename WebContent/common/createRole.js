document.write("<script type='text/javascript' src='../common/feature.js'></script>");
document.write("<script type='text/javascript' src='../parameter.js'></script>");
(function($) {
	$(function() {
		$("#submitBtn").click(
				function() {
					if (roleValidateForm()) {
						$.ajax({
							type : "POST",
							contentType : 'application/json',
							url : restful.roleInsert,
							dataType : "json",
							data : $("#CreateRole").serialize(),
							success : function(data, response, xhr) {
								if (xhr.status == "200"
										&& data.status == "Success") {
									location.href = 'roleList.html';
								} else {
									alert("status: " + xhr.status + ", "
											+ xhr.statusText
											+ " ,exception message: "
											+ data.exceptionMessage);
								}
							},
							error : function(xhr) {
								alert("err: " + xhr.status + ' '
										+ xhr.statusText);
							}
						});
					}
				});

		$('#CreateRole').attr('action', restful.roleInsert);

		$("#goToPersonsList").click(function() {
			location.href = '../persons/personList.html';
		});
		$("#goToRolesList").click(function() {
			location.href = 'roleList.html';
		});
	});
}(jQuery));

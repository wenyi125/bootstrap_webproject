document.write("<script type='text/javascript' src='../common/feature.js'></script>");
document.write("<script type='text/javascript' src='../parameter.js'></script>");
(function($) {
	$(function() {
		var para = getPara();
		$("#roleId").val(para[0][1]);
		$("#roleName").val(para[1][1]);
		$('#maintainRoleForm').attr('action', restful.roleUpdate);

		$("#maitainRoleSubmit").click(
				function() {
					if (roleValidateForm()) {
						$.ajax({
							type : "POST",
							url : restful.roleUpdate,
							contentType : 'application/json',
							data : $("#maintainRoleForm").serialize(),
							dataType : "json",
							success : function(data, response, xhr) {
								if (xhr.status == "200"
										&& data.status == "Success") {
									location.href = 'roleList.html';
								} else {
									alert("status: " + xhr.status + ", "
											+ xhr.statusText
											+ ", exception message: "
											+ data.exceptionMessage);
								}
							},
							error : function(xhr) {
								alert("err: " + xhr.status + ' '
										+ xhr.statusText);
							}
						});
					}
				});
		$("#maitainRoleReset").click(function() {
			roleFormReset();
		});
	});
}(jQuery));
function roleFormReset() {
	var para = getPara();
	$("#roleId").val(para[0][1]);
	$("#roleName").val("");
}

document.write("<script type='text/javascript' src='../common/feature.js'></script>");
document.write("<script type='text/javascript' src='../parameter.js'></script>");
(function($) {
	$(function() {
		$.ajax({
			type : "GET",
			url : restful.peopleGetRoles,
			dataType : "json",
			success : function(data, response, xhr) {
				if (xhr.status == "200" && data.status == "Success") {
					showRoles(data);
				} else {
					alert("status: " + xhr.status + ", " + xhr.statusText
							+ " ,exception message: " + data.exceptionMessage);
				}
			},
			error : function(xhr) {
				alert("err: " + xhr.status + ' ' + xhr.statusText);
			}
		});

		$("#submitBtn").click(
				function() {
					if (personValidateForm()) {
						$.ajax({
							type : "POST",
							contentType : 'application/json',
							url : restful.peopleInsert,
							data : $("#CreatePerson").serialize(),
							success : function(data, response, xhr) {
								if (xhr.status == "200"
										&& data.status == "Success") {
									location.href = 'personList.html';
								} else {
									alert("status: " + xhr.status + ", "
											+ xhr.statusText
											+ " ,exception message: "
											+ data.exceptionMessage);
								}
							},
							error : function(xhr) {
								alert("err: " + xhr.status + ' '
										+ xhr.statusText);
							}
						});
					}
				});

		$('#CreatePerson').attr('action', restful.peopleInsert);

		$("#goToPersonsList").click(function() {
			location.href = 'personList.html';
		});

		$("#goToRolesList").click(function() {
			location.href = '../roles/roleList.html';
		});

	});
}(jQuery));

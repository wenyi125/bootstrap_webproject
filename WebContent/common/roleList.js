document.write("<script type='text/javascript' src='../parameter.js'></script>");
(function($) {
	$(function() {
		getListOfRoles();
		$("#goToCreateRole").click(function() {
			location.href = 'createRole.html';
		});
		$("#goToListOfPersons").click(function() {
			location.href = '../persons/personList.html';
		});
		$("#homePage").click(function() {
			location.href = '../index.html';
		});
	});
}(jQuery));
function getListOfRoles() {
	$.ajax({
		type : "GET",
		url : restful.roleList,
		dataType : "json",
		success : function(data, response, xhr) {
			if (xhr.status == "200" && data.status == "Success") {
				getRolesTable(data);
			} else {
				alert("status: " + xhr.status + ", " + xhr.statusText
						+ " ,exception message: " + data.exceptionMessage);
			}
		},
		error : function(xhr) {
			alert("err: " + xhr.status + ' ' + xhr.statusText);
		}
	});
}
function deleteRole(roleId) {
	$.ajax({
		type : "POST",
		url : restful.roleDelete + roleId,
		dataType : "json",
		success : function(data, response, xhr) {
			if (xhr.status == "200" && data.status == "Success") {
				$("#rolesTable").find("tr:gt(0)").remove();
				getListOfRoles();
			} else if (xhr.status == "200" && data.status == "Fail") {
				alert(data.data);
			} else {
				alert("status: " + xhr.status + ", " + xhr.statusText
						+ " ,exception message: " + data.exceptionMessage);
			}
		},
		error : function(xhr) {
			alert("err: " + xhr.status + ' ' + xhr.statusText);
		}
	});
}

function getRolesTable(data) {
	var str = "<tr><td> </td><td> </td><td> </td></tr>";
	var roles = JSON.parse(data.data);
	$.each(roles, function(index, v) {
		$('#rolesTable').append(str);
		var $specifyTd = $('#rolesTable tr:last').find('td');
		var maintainRoleHref = "maintainRole.html?roleId=" + v.roleId
				+ "&roleName=" + v.roleName
		var h = $("<a>", {
			href : maintainRoleHref,
			text : v.roleId
		});
		var b = $('<button/>', {
			text : 'delete',
			name : 'delete',
			id : v.roleId,
			on : {
				click : function() {
					deleteRole(v.roleId);
				}
			}
		});
		$specifyTd.eq(0).append(h);
		$specifyTd.eq(1).text(v.roleName);
		$specifyTd.eq(2).append(b);
	});
}
document.write("<script type='text/javascript' src='../parameter.js'></script>");
(function($) {
	$(function() {
		getListOfPersons();
		$("#goToCreatePerson").click(function() {
			location.href = 'createPerson.html';
		});
		$("#goToListOfRoles").click(function() {
			location.href = '../roles/roleList.html';
		});
		$("#homePage").click(function() {
			location.href = '../index.html';
		});
	});
}(jQuery));
function getListOfPersons() {
	$.ajax({
		type : "GET",
		url : restful.peopleList,
		dataType : "json",
		success : function(data, response, xhr) {
			if (xhr.status == "200" && data.status == "Success") {
				getPersonsTable(data);
			} else {
				alert("status: " + xhr.status + ", " + xhr.statusText
						+ " ,exception message: " + data.exceptionMessage);
			}
		},
		error : function(xhr) {
			alert("err: " + xhr.status + ' ' + xhr.statusText);
		}
	});
}

/* 列出 DB 中所有的 person */
function getPersonsTable(data) {
	var persons = JSON.parse(data.data);
	$
			.each(
					persons,
					function(index, v) {
						var str = "<tr><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>";
						$('#personsTable').append(str);
						var $specifyTd = $('#personsTable tr:last').find('td');
						var roleId = [];
						var roleName = [];
						for (var i = 0; i < v.personRoleRelation.length; i++) {
							roleId[i] = v.personRoleRelation[i].role.roleId
							roleName[i] = v.personRoleRelation[i].role.roleName
						}
						var maintainPersonHref = "maintainPerson.html?personId="
								+ v.personId
								+ "&firstName="
								+ v.firstName
								+ "&lastName=" + v.lastName;
						var h = $("<a>", {
							href : maintainPersonHref,
							text : v.personId
						});
						var b = $('<button/>', {
							text : 'delete',
							name : 'delete',
							id : v.personId,
							on : {
								click : function() {
									deletePerson(v.personId);
								}
							}
						});
						$specifyTd.eq(0).append(h);
						$specifyTd.eq(1).text(v.firstName);
						$specifyTd.eq(2).text(v.lastName);
						$specifyTd.eq(3).text(roleId);
						$specifyTd.eq(4).text(roleName);
						$specifyTd.eq(5).append(b);
					});
}

function deletePerson(personId) {
	$.ajax({
		type : "GET",
		url : restful.peopleDelete + personId,
		dataType : "json",
		success : function(data, response, xhr) {
			if (xhr.status == "200") {
				if (data.status == "Success") {
					$("#personsTable").find("tr:gt(0)").remove();
					getListOfPersons();
				} else {
					alert(data.exceptionMessage);
				}
			} else {
				alert("status: " + xhr.status + ", " + xhr.statusText
						+ " ,exception message: " + data.exceptionMessage);
			}
		},
		error : function(xhr) {
			alert("err: " + xhr.status + ' ' + xhr.statusText);
		}
	});
}
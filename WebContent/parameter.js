
var restful = {"peopleList":"../webapi/people/list",
					"peopleDelete":"../webapi/people/delete/",
					"peopleGetRoles":"../webapi/people/getRoles",
					"peopleInsert":"../webapi/people",
					"peopleGetPersonRoles":"../webapi/people/maintain/getPersonRoles/",
					"peopleUpdate":"../webapi/people/maintain",
					"roleInsert":"../webapi/role",
					"roleUpdate":"../webapi/role/maintain",
					"roleList":"../webapi/role/list",
					"roleDelete":"../webapi/role/delete/"};
